package com.example.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.petcare.Adapters.CellAdapter;
import com.example.petcare.Model.Task;
import com.example.petcare.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private CellAdapter cellAdapter;
    private ArrayList<Task> TaskList;

    private Button buttonSave, buttonDelete;
    private EditText inputTask;

    FirebaseDatabase database;
    DatabaseReference ref;
    private String FamilyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonSave = findViewById(R.id.buttonSave);
        buttonDelete = findViewById(R.id.buttonDelete);
        inputTask = findViewById(R.id.inputTask);
        database = FirebaseDatabase.getInstance("https://petcare-9407b-default-rtdb.europe-west1.firebasedatabase.app/");
        ref = database.getReference();

        TaskList = new ArrayList<>();  // recycler
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        getFamilyNamedb();
        readTask();

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task = inputTask.getText().toString();

                writeNewTask(task);
            }
        });
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Query query = FirebaseDatabase.getInstance("https://petcare-9407b-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("tasks").child("ztr");

                query.getRef().removeValue();
            }
        });
    }

    private void readTask() {
        Query query = FirebaseDatabase.getInstance("https://petcare-9407b-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("tasks").child("ztr");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                TaskList.clear();
                for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Task task = snapshot1.getValue(Task.class);
                    TaskList.add(task);
                }
                cellAdapter = new CellAdapter(getApplicationContext(), TaskList);
                mRecyclerView.setAdapter(cellAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void writeNewTask(String task) {
        String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        Task taskDb = new Task(task, currentTime);

        ref.child("tasks").child("ztr").push().setValue(taskDb);
    }

    private void getFamilyNamedb() {
        final FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference userTest = FirebaseDatabase.getInstance("https://petcare-9407b-default-rtdb.europe-west1.firebasedatabase.app/").getReference("users").child(fUser.getUid()).child("familyName");
        userTest.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                FamilyName = new String();
                FamilyName = snapshot.getValue(String.class);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), StartActivity.class));
    }

}