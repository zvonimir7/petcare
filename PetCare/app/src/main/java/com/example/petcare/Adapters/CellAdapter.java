package com.example.petcare.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.petcare.CellName;
import com.example.petcare.Model.Task;
import com.example.petcare.R;

import java.util.ArrayList;
import java.util.List;

public class CellAdapter extends RecyclerView.Adapter<CellAdapter.ViewHolder> {

    private Context mContext;
    private List<Task> mTask;


    public CellAdapter(Context mContext, List<Task> mTask) {
        this.mTask = mTask;
        this.mContext = mContext;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cell_name, parent, false);
        return new CellAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        holder.mtask.setText(mTask.get(position).getTask());
        holder.mtime.setText(mTask.get(position).getTime());


    }

    @Override
    public int getItemCount() {
        return mTask.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mtask;
        public TextView mtime;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mtask = itemView.findViewById(R.id.textViewcCell);
            mtime = itemView.findViewById(R.id.textViewtime);


        }

    }


}