package com.example.petcare.Model;

public class Task {
    public String task;
    public String time;

    public Task(String task, String time) {
        this.task = task;
        this.time = time;
    }
    public Task() {

    }

    public String getTask() {
        return task;
    }

    public String getTime() {
        return time;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

