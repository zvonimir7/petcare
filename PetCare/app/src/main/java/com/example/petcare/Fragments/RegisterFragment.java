package com.example.petcare.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petcare.MainActivity;
import com.example.petcare.R;
import com.example.petcare.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterFragment extends Fragment {

    EditText FullName, Email, Password,Family;
    Button RegisterBtn;

    FirebaseAuth fAuth;


    FirebaseDatabase database;
    DatabaseReference ref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        initializeUI(view);
        return view;
    }

    private void initializeUI(View view) {
        FullName = view.findViewById(R.id.editTextFullName);
        Email = view.findViewById(R.id.editTextEmail);
        Password = view.findViewById(R.id.editTextPassword);
        RegisterBtn = view.findViewById(R.id.buttonRegister);
        Family = view.findViewById(R.id.editTextFamily);



        fAuth = FirebaseAuth.getInstance();
        if (fAuth.getCurrentUser() != null) {
            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = Email.getText().toString().trim();
                String password = Password.getText().toString().trim();
                String fullName = FullName.getText().toString().trim();
                String family= Family.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Email.setError("Email is required");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Password.setError("Password is required");
                    return;
                }
                if (password.length()<6) {
                    Password.setError("Password must be longer then 6 characters");
                    return;
                }
                if (TextUtils.isEmpty(family)) {
                    Email.setError("Email is required");
                    return;
                }

                //REGSTERING IN FIREBASE
                fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "User has been created", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            //DODAJ UBACIVANJE U DATABASE PO EMAILU
                            // user=new User(email);
                            database = FirebaseDatabase.getInstance("https://petcare-9407b-default-rtdb.europe-west1.firebasedatabase.app/");
                            ref= database.getReference();

                            writeNewUser(email,fullName,family,password);


                        } else {
                            Toast.makeText(getContext(), "User has not been created", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }


    public void writeNewUser(String email,String fullName,String familyName,String password) {
        String fUser=fAuth.getCurrentUser().getUid();
        User user = new User(email,fullName,familyName,password,fUser);


         ref.child("users").child(fAuth.getCurrentUser().getUid()).setValue(user);
       //ref.child("users").child("emaillllllll").setValue(user);
    }

}

