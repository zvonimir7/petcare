package com.example.petcare.Model;

//   package com.google.firebase.referencecode.database.models;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {


    public String email;
    public String fullName;
    public String familyName;
    public String password;
    public String myID;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String email,String fullName,String familyName,String password,String myID) {

        this.email = email;
        this.fullName = fullName;
        this.familyName= familyName;
        this.password= password;
        this.myID=myID;
    }

    public String getmyID() {
        return myID;
    }

    public void setmyID(String myID) {
        this.myID = myID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

